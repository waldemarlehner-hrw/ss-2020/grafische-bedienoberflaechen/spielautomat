package view;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Spielautomat;

public class ModalHelper {
    public static void CreateWinModal(int wonCount, Stage parent){
        var modal = new VBox();
        var button = new Button("Schliessen");
        var pad = new Insets(15,15,15,15);
        modal.setSpacing(40);
        modal.setPadding(pad);
        modal.getChildren().add(new Label(String.format("Du hast %d gewonnen!", wonCount)));
        modal.getChildren().add(button);
        var modalScene = new Scene(modal);
        var stage = new Stage(StageStyle.UTILITY);
        stage.setScene(modalScene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(parent);
        button.setOnAction((action) -> stage.close());

        stage.showAndWait();

    }

    public static void CreateOutOfMoneyModal(Stage parent) {
        var modal = new VBox();
        var button = new Button("Schliessen");
        var pad = new Insets(15,15,15,15);
        modal.setSpacing(40);
        modal.setPadding(pad);
        modal.getChildren().add(new Label(String.format("Herzlichen Glueckwunsch! Dank deiner Spielsucht bist du Pleite!\nDu kannst dir den Einsatz von %d nicht mehr leisten.", Spielautomat.getEinsatz())));
        modal.getChildren().add(button);
        var modalScene = new Scene(modal);
        var stage = new Stage(StageStyle.UTILITY);
        stage.setScene(modalScene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(parent);
        button.setOnAction((action) -> stage.close());

        stage.showAndWait();
    }
}
