package view;

import ViewModel.ModelListener;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Spielautomat;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        var listener = new ModelListener();
        Spielautomat.setEinsatz(5);

        Spielautomat.setStartGuthaben(listener.getProperties().getguthaben());
        Parent root = FXMLLoader.load(getClass().getResource("./main.fxml"));
        primaryStage.setTitle("Gambling Box");

        var scene = new Scene(root, 200, 275);
        scene.getStylesheets().add(getClass().getResource("./main.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}