package view;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import model.Spielautomat;
import model.spielautomatEvent;
import model.spielautomatListener;

import java.net.URL;
import java.util.ResourceBundle;


public class Controller implements Initializable {
    @FXML private TextField money;
    @FXML private TextField winnings;
    @FXML private HBox output;
    @FXML private Button start;
    @FXML private Button stop;

    @FXML private final IntegerProperty IntMoney = new SimpleIntegerProperty(0);

    @FXML private final IntegerProperty IntWinnings = new SimpleIntegerProperty(0);
    private final BooleanProperty isRolling = new SimpleBooleanProperty(false);


    public Controller(){}

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        start.setOnAction(actionEvent -> {
            if(!isRolling.get()){
                // Check can afford
                if(Spielautomat.getEinsatz() > Spielautomat.getInstance().getGuthaben()){
                    ModalHelper.CreateOutOfMoneyModal( (Stage)output.getScene().getWindow());
                    return;
                }

                Spielautomat.getInstance().startRollen();
                isRolling.setValue(true);
            }
        });
        stop.setOnAction((actionEvent) -> {
            if (isRolling.get()) {
                Spielautomat.getInstance().stopRollen();
                isRolling.setValue(false);
            }
        });

        this.IntMoney.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number _old, Number _new) {
                money.setText(_new.toString());
            }
        });

        this.IntWinnings.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number _old, Number _new) {
                winnings.setText(_new.toString());
            }
        });

        this.isRolling.addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean _old, Boolean _new) {
                start.setDisable(_new);
                stop.setDisable(!_new);
            }
        });

        this.IntMoney.set(Spielautomat.getInstance().getGuthaben());
        this.IntWinnings.set(Spielautomat.getInstance().getGewinn());
        Spielautomat.getInstance().addspielListener(new spielautomatListener() {
            @Override
            public void ModelChanged(spielautomatEvent e) {
                var numbers = e.getZiffern();
                var fields = output.getChildren();
                for(int i = 0; i < fields.size(); i++){
                    var textField = (Label)fields.get(i);
                    int num = numbers[i];
                    Platform.runLater(()->{
                        textField.setText(String.format("%d",num));
                    });
                }
            }

            @Override
            public void StatusChanged(spielautomatEvent e) {
                if(e.getStatus()){
                    var winnings = e.getGewinn();
                    ModalHelper.CreateWinModal(winnings, (Stage)output.getScene().getWindow());
                    IntWinnings.set(Spielautomat.getInstance().getGewinn() + IntWinnings.get());

                }

                IntMoney.set(Spielautomat.getInstance().getGuthaben());
            }
        });
    }
}
