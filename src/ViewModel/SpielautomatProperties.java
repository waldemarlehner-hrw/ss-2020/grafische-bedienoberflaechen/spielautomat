package ViewModel;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Spielautomat;

import java.util.ArrayList;

public class SpielautomatProperties 
{

	
	 private BooleanProperty status = new SimpleBooleanProperty(!Spielautomat.GEWONNEN);
	 
	 private  IntegerProperty guthaben=new SimpleIntegerProperty(0);
	 private  IntegerProperty einsatz=new SimpleIntegerProperty(0);
	 private  IntegerProperty gewinn=new SimpleIntegerProperty(0);
	
	
	// Properties f�r das Ziffern-Array
	   ObservableList<Integer> observableList = FXCollections.observableArrayList(new ArrayList<Integer>());
	   private ListProperty<Integer> ziffern = new SimpleListProperty<Integer>(observableList);
	   
	   
	    // Define a getter for the property's value
	    public final Integer getguthaben(){return guthaben.getValue();}
	    public final Integer geteinsatz(){return einsatz.getValue();}
	    public final Integer getgewinn(){return gewinn.getValue();}
	    public final Boolean getstatus(){return status.getValue();}
	    
	    // Define a setter for the property's value
	    public final void setguthaben(Integer guthaben){this.guthaben.setValue(guthaben);}
	    public final void seteinsatz(Integer anzahlVerschiebungen){this.einsatz.setValue(anzahlVerschiebungen);}
	    public final void setgewinn(Integer gewinn){this.gewinn.setValue(gewinn);}
	    public final void setstatus(Boolean gewonnen){this.status.setValue(gewonnen);}
	    
	     // Define a getter for the property itself
	    public IntegerProperty guthabenProperty() {return guthaben;}
	    public IntegerProperty einsatzProperty() {return einsatz;}
	    public IntegerProperty gewinnProperty() {return gewinn;}
	    public BooleanProperty statusProperty() {return status;}
	    public ListProperty<Integer> ziffernProperty() { return ziffern; }

}
