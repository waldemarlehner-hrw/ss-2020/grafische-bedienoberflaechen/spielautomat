package ViewModel;
import model.Spielautomat;
import model.spielautomatEvent;
import model.spielautomatListener;


public class ModelListener implements spielautomatListener
{

	private SpielautomatProperties viewModel= new SpielautomatProperties();
	
	@Override
	public void ModelChanged(spielautomatEvent e) 
	{  
		Spielautomat spiel = (Spielautomat) e.getSource();
		
		viewModel.setgewinn(e.getGewinn());
		viewModel.setguthaben(spiel.getGuthaben());

		// Propertieliste aktualisieren
		int[] ziffern=e.getZiffern();
		
	     viewModel.ziffernProperty().clear(); 
		 for ( int i=0; i<ziffern.length; i++)
         { 
			  viewModel.ziffernProperty().add(new Integer(ziffern[i]));                     
         } 
	}

	@Override
	public void StatusChanged(spielautomatEvent e) 
	{    
		  Spielautomat spiel = (Spielautomat) e.getSource();
	      viewModel.setgewinn(e.getGewinn());
	      viewModel.setguthaben(spiel.getGuthaben());
	      viewModel.setstatus(e.getStatus());

	}
	
	public SpielautomatProperties getProperties()
	{
		return viewModel;
	}

}
