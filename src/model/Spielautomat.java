package model;
import java.util.Random;
import java.util.Vector;

public class Spielautomat
{   
	// Globale Konstanten
	/** Die Anzahl der Ziffern des Spielautomaten 
	 * 	
	 */
	public static final int ANZAHLZIFFERN=4;
	/** Der Starteinsatz f�r ein Spiel 
	 * 	
	 */
	public static final int STARTEINSATZ=5;
	/** Das Startguthaben 
	 * 	
	 */
	public static final int STARTGUTHABEN=100;
	/**  Diese Konstante enth�lt den Wert f�r den Status Spiel gewonnen
	 * 	
	 */
	public static final boolean GEWONNEN=true;
	
	// Liste f�r registrierte Listener
	private Vector<spielautomatListener> subscribers = new Vector();
	
	private Integer zufall[]= new Integer[ANZAHLZIFFERN];  // 4 zuf�llige Zahlen
	
	private boolean status = !GEWONNEN;
	private boolean isRunning = false;
	
	private int kategorie = 1;					// Gewinnkatagorie 1: kein Gewinn, 2, 3, 4 unterschiedliche Gewinne
	private int gewinn = 0;						// Gewinn
	//private int[] mehrfachZiffer = {-1,-1};     // es sind maximal 2 Mehrfachhziffern m�glich
	
	private static int guthaben=0;
	private static int einsatz=0;
	
	// Singleton
	private static Spielautomat me = null;
	
	// Thread f�r das Rollen der Ziffern
	private static Thread thread= null;
	
	
	/** Der Spielautomat ist nach dem Entwurfsmuster singleton implementiert
	 * Eine Instanz des Speilautomates wird daher nicht mit new, sondern �ber die Klassenmethode getInstance erzeugt
	 * 	
	 * @return:  Liefert die Instanz des Spielautomaten zur�ck
	 */
	public static Spielautomat getInstance()
	{ if ( me == null )
	  {    me = new Spielautomat();
	  }	
	  return me;
	}
	
	// Konstruktor kann nur �ber getInstance aufgerufen werden
	private Spielautomat()
	{   // Der Spielautomat startet mit 4 zuf�lligen Ziffern
		me=this;
		System.out.println("spielautomatInstanz wird erzeugt. Ensatz="+einsatz);
		if (einsatz==0) einsatz=STARTEINSATZ;
		if (guthaben==0) guthaben=STARTGUTHABEN;
		spielen(); 
	}
    
	private class spielThread extends Thread

	{    Spielautomat spiel;

	     spielThread(Spielautomat spiel){this.spiel = spiel;}
	     
		 public void run()
		    {	System.out.println("Spielthread is running");
		    	while ( !this.isInterrupted())
		    	{
		    		spiel.spielen();
		    		try {
						Thread.sleep(100);
					} catch (InterruptedException e) { System.out.println("Thread interrupted"); break; }
		    	}
		    }
	}
	// get- und set-Methoden

	/** 
	 * 	setzt das Startguthaben f�r den Spielstart
	 *  Muss gesetzt werden vor Aufruf von getInstance
	 * 
	 * @param: int Startguthaben
	 */
    public static void setStartGuthaben(int wert){ if (guthaben==0) Spielautomat.guthaben=wert;}
    
    /** 
	 * 	Liefert das aktuelle Guthaben
	 * 
	 * @return:  aktuelles Guthaben
	 */
    
    public int getGuthaben(){return guthaben; }
	/** 
	 * 	�ndert den Einsatz f�r ein Spiel
	 *  Kann nur einmal vor dem Start eines Spiels gesetzt werden
	 * 
	 * @param:   Neuer Einsatz f�r ein Spiel
	 * 
	 */
    public static void setEinsatz(int wert){ Spielautomat.einsatz = wert; }
    
	/** 
	 * 	Liefert den Einsatz je Spielrunde
	 *  Muss gesetzt werden vor Aufruf von getInstance
	 * 
	 * @return:  int .  Einsatz f�r eine Speilrunde
	 */
    
  public static int getEinsatz(){ return einsatz; }
        
	/** 
	 * 	Liefert den aktuellen Gewinn
	 * 
	 * @return:  aktueller Gewinn
	 */
    public int  getGewinn(){ return gewinn; }
    
	/** 
	 * 	Liefert die aktuellen 4 Ziffern des Spielautomaten
	 * @return:  array mit 4 Integer-Objekten
	 */
    public Integer[] getZiffern(){ return zufall; }
    
	/** 
	 * 	Liefert die aktuellen 4 Ziffern des Spielautomaten
	 * @return:  array mit 4 int-Werten
	 */
    
    public  int[] getIntZiffern()
    {   int[] ziffern= new int[ANZAHLZIFFERN];
    	for ( int i = 0; i<ANZAHLZIFFERN; i++) ziffern[i] = zufall[i].intValue();
    	return ziffern;
    }
    
    // 4 zuf�llige Ziffern errechnen
    protected void spielen()
    {
    	Random  zufallszahl = new Random();
	
	    for ( int i=0; i<ANZAHLZIFFERN; i++) 
	    {   zufall[i]=new Integer(zufallszahl.nextInt(10));	 }
	    
	    this.onModelChanged();  // Meldung an View, dass neue Ziffern berechnet wurden
    }
    
    // Thread zum rollen der Ziffern starten
	/** 
	 * 	Startet den Spielautomaten. In einem Thread werden fortlaufend 4 neue Ziffern zuf�llig berechnet
	 *  Wirft spielautomatenEvents bei jeder neuen Ziffernfolge und bei �nderung des Guthabens
	 */
    public void startRollen()
    {   gewinn = 0;
        kategorie=1;
        System.out.println("startRollen");
        if ( isRunning == false)
        { 
           guthaben=guthaben-einsatz;    
           status=!GEWONNEN;
           System.out.println("Status event versenden");
           this.onGameStatus();   // View  �ber neu berechnetes Guthaben informieren
           
       
           
           isRunning = true;
           if (thread == null) 
           { thread = new spielThread(me); 
             thread.start(); 
           } 
           
        }
    }
    // Thread zum rollen der Ziffern anhalten
    /** 
	 * 	Stopt den Spielautomaten. 
	 *  Ermittelt den Gewinn und errechnet das neue Guthaben
	 *  Wirft spielautomatenEvents 
	 */
  public void stopRollen()
    {   System.out.println("stopRollen");
    	if (isRunning == true)
        {   isRunning = false;	
    	    if ( thread != null ) thread.interrupt();
            thread = null;
    	    BerechneGuthaben();
        }
        
    }
    
    
    private void BerechneGuthaben()
    {   
   	 
   	 // Berechne Gewinn
   	 int zaehler=1; //  Wie oft kommt eine Zahl vor?
 //  	 mehrfachZiffer[0]=-1;
 //  	 mehrfachZiffer[1]=-1;
    
   	 for ( int i=0; i<ANZAHLZIFFERN-1; i++)
   	 { 
   	   if (zaehler < ANZAHLZIFFERN-1 )
   	      for( int v=i+1; v<ANZAHLZIFFERN; v++) if (zufall[i].intValue() == zufall[v].intValue()) 
   	                                  { zaehler++;
   	                                    
   	                                     /* if(mehrfachZiffer[0]==-1) mehrfachZiffer[0]=zufall[i];
   	                                     else mehrfachZiffer[1] = zufall[i]; */
   	                                  }
   	  
   	 }
   	 
   	 kategorie = zaehler;	 
   	 switch (zaehler)
   	 {
   	    case 1: gewinn=0; status=!GEWONNEN; break;        // Gewinnkategorie 1
   	    case 2: gewinn=5; status=GEWONNEN;  break;        // Gewinnkategorie 2
   	    case 3: gewinn=100; status= GEWONNEN;  break;     // Gewinnkategorie 3
   	    case 4: gewinn=1000; status = GEWONNEN; break;    // Gewinnkategorie 4
   	 }
   	 
   	 guthaben = guthaben+gewinn;
   	 
     this.onGameStatus();   // View  �ber neu berechnetes Guthaben informieren
    }
    
   
    
    /**
     * Registriert einen spielautomatListener bei der Spielelogik
     */
    public void addspielListener(spielautomatListener listener)
    {
    	// nur hinzuf�gen, wenn noch nicht vorhanden...
    	if(!this.getSubscribers().contains(listener))
    		this.getSubscribers().add(listener); 
    }

    private Vector<spielautomatListener> getSubscribers(){return this.subscribers;}
    
    private void setSubscribers(Vector<spielautomatListener> subscribers)
    {
    	this.subscribers = subscribers;
    }

    // Wenn sich auf dem Spielfeld was ver�ndert hat
    private void onModelChanged()
    {
    	for(spielautomatListener s : this.getSubscribers())
    	{
    		s.ModelChanged(new spielautomatEvent(this, getIntZiffern(), status, gewinn, kategorie /*,  mehrfachZiffer*/));
    	}
    }

    /**
     * Sendet ein spielautomatEvent an die spielautomatListener, falls ein Gewinn erzielt  oder das Guthaben ver�ndert wurde 
     * 
     */

    // Wenn sich der Zusand vom Spiel ge�ndert hat
    private void onGameStatus()
    {	
    	for(spielautomatListener s : this.getSubscribers())
    	{
    		s.StatusChanged(new spielautomatEvent(this, getIntZiffern(), status, gewinn, kategorie /*, mehrfachZiffer */));
     	}
    }
}


