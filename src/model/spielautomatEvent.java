package model;
import java.util.EventObject;
/**
	 * Ein spielautomatEvent, der von der Klasse spielautomat erzeugt wird
	 * 
	 * @autor:  Prof. Dr.-Ing- Silvia Keller
	 */
public class spielautomatEvent extends EventObject
{       // Kategorie zeigt an wie oft eine Zahl mehrfach vorkommt
	    // status
	    protected int ziffern[], gewinn=0, kategorie=1;
	    //protected int mehrfachZiffer[];
	    protected boolean status= false;
	    
		spielautomatEvent(Spielautomat source, int ziffern[], boolean status, int gewinn, int kategorie /*, int mfZiffer[] */)
		{
			super(source);
			this.ziffern=ziffern;
			this.status=status;
			this.gewinn = gewinn;
			this.kategorie = kategorie;
			//this.mehrfachZiffer = mfZiffer;
			
		}

		/**
		 * Liefert die aktuellen Ziffern beim Eintritt des Events
		 * 
		 * @return  array mit int-Werten
		 */
		
		public int[] getZiffern(){return ziffern;}
		/**
		 * Liefert den Spielstatus <br/>
		 * Ob ein Gewinn vorliegt kann mit der Konstanten GEWONNEN abgefragt werden
		 * 
		 * @return  status des Spiels. Der Status zeigt an, ob ein Gewinn erzielt wurde oder nicht
		 */
		public boolean getStatus(){return status;}
		/**
		 * Liefert den Gewinn bei Eintritt des Events
		 * 
		 * @return  Gewinn
		 */
		public int getGewinn(){return gewinn;}
		/**
		 * Liefert die Gewinnkategorie
		 * 
		 * @return  Gewinnkategorie <br/> 1:  kein Gewinn <br/> 2:  Gewinn ist 5� <br/> 3: Gewinn ist 100�<br/> 4:  Gewinn ist 1000�
		 */
		public int getKategorie(){return kategorie;}
		//public int[] getmfZiffer(){return mehrfachZiffer;}
}
